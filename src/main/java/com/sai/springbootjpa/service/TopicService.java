package com.sai.springbootjpa.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.sai.springbootjpa.model.Topic;

@Service
public class TopicService {
	private List<Topic> topics = new ArrayList<>();
	{
		topics.addAll(Arrays.asList(
				new Topic(1, "java", "1.x"), 
				new Topic(2, "dotnet", "2.x"),
				new Topic(3, "python", "3.x")));
	}

	public List<Topic> getTopics() {
		return topics;
	}

	public Optional<Topic> getTopic(Integer topicId) {
		return topics.stream().filter(x -> x.getId().equals(topicId)).findFirst();
	}

	public void addTopic(Topic topic) {
		topics.add(topic);
	}

	public void updateTopic(Topic topic) {
		for (int i = 0; i < topics.size(); i++) {
			Topic currTopic = topics.get(i);
			if (currTopic.getId().equals(topic.getId())) {
				currTopic.setName(topic.getName());
				currTopic.setDesc(topic.getDesc());
			}
		}
	}

	public void deleteTopic(Integer topicId) {
		topics.removeIf(x -> x.getId().equals(topicId));
	}
}
