package com.sai.springbootjpa.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sai.springbootjpa.model.Topic;
import com.sai.springbootjpa.service.TopicService;

@RestController
public class TopicController {

	@Autowired
	private TopicService topicService;

	@GetMapping("/topics")
	public List<Topic> getTopics() {
		return topicService.getTopics();
	}

	@GetMapping("/topics/{topicId}")
	public Topic getTopic(@PathVariable Integer topicId) {
		Optional<Topic> topic = topicService.getTopic(topicId);
		return topic.isPresent() ? topic.get() : null;
	}

	@PostMapping("/topics")
	public void addTopic(@RequestBody Topic topic) {
		topicService.addTopic(topic);
	}

	@PutMapping("/topics")
	public void updateTopic(@RequestBody Topic topic) {
		topicService.updateTopic(topic);
	}

	@DeleteMapping("/topics/{topicId}")
	public void deleteTopic(@PathVariable Integer topicId) {
		topicService.deleteTopic(topicId);
	}
}
